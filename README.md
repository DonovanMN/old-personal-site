# Personal site for Donovan Mikrot

This is an HTML5 application, built with [Brunch](http://brunch.io).

## Getting started
* Install (if you don't have them):
    * [Node.js](https://nodejs.org/en/download/)
    * [Yarn](https://yarnpkg.com/en/docs/install)
    * Run yarn to install Brunch, plugins, and other dependencies: `yarn`.
* Run:
    * `yarn run start` — watches the project with continuous rebuild. This will also launch HTTP server with [pushState](https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Manipulating_the_browser_history).
    * `yarn run build` — builds minified project for production
* Learn:
    * `public/` dir is fully auto-generated and served by HTTP server.  Write your code in `app/` dir.
    * Place static files in `app/assets/` to be copied into `public/` by the build.
    * [Brunch site](http://brunch.io)
* Publish:
    * Changes pushed or merged into `master` are automatically built and deployed to GitLab pages

### License
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
