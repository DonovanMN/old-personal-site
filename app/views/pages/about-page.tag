<about-page>
  <h3><i class="material-icons small">info</i> About Donovan</h3>
  <dm-intro></dm-intro>
  <dm-opinions></dm-opinions>
  <dm-contact></dm-contact>
</about-page>

<dm-intro>
  <div class="card-panel">
    <p class="flow-text">
      I am a software developer and self-proclaimed nerd. I will never stop learning new things.
      I want to help sustainably fight climate change. I am politically liberal and open-minded.
      I am very enthusiastic about technology and how it can change the world.
    </p>
  </div>

  <style scoped>
    .card-panel > .flow-text {
      margin: 0;
    }
  </style>
</dm-intro>

<dm-opinions>
  <div class="card-panel">
    <h5><i class="material-icons">thumbs_up_down</i> Likes and Dislikes</h5>
    <table class="highlight">
      <thead>
        <tr>
          <th>Category</th>
          <th>Likes</th>
          <th>Dislikes</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Food</td>
          <td>
            <a href="https://reducetarian.org/" target="_blank">Less meat</a>,
            <a href="http://theoatmeal.com/comics/sriracha" target="_blank">Spicy</a> food of any variety,
            <a href="http://vimeo.com/103559925" target="_blank">Eggs</a>
          </td>
          <td>Added Sugar, Steamed Carrots, Asparagus</td>
        </tr>
        <tr>
          <td>Exercise</td>
          <td>Biking, <a href="http://ragbrai.com/" target="_blank">RAGBRAI</a>, Swimming, Weightlifting</td>
          <td>Running</td>
        </tr>
        <tr>
          <td>Places</td>
          <td>Germany, Greece, Mexico, India, Scotland, USA</td>
          <td>North Korea</td>
        </tr>
        <tr>
          <td>Transportation</td>
          <td>Biking, Walking, <a href="https://www.metrotransit.org/" target="_blank">Public Transit</a></td>
          <td>Horseback, <a href="http://en.wikipedia.org/wiki/The_Oregon_Trail_%28video_game%29" target="_blank">Covered Wagon</a></td>
        </tr>
        <tr>
          <td>Books</td>
          <td>Tao Te Ching, Zen and the Art of Motorcycle Maintenance, Ender Series, The Hitchhiker’s Guide to the Galaxy</td>
          <td>
            <a href="http://www.amazon.com/Sarah-Palin/e/B002QW385O" target="_blank">Any of these.</a>
            <a href="http://www.amazon.com/Rush-Limbaugh/e/B000APZECY" target="_blank">Or these.</a>
          </td>
        </tr>
        <tr>
          <td>Movies</td>
          <td><a href="http://www.imdb.com/title/tt0111161" target="_blank">The Shawshank Redemption</a></td>
          <td><a href="http://www.imdb.com/title/tt0185183" target="_blank">Battlefield Earth</a></td>
        </tr>
        <tr>
          <td>Directors</td>
          <td>Christopher Nolan, Wong Kar Wai, Akira Kurosawa</td>
          <td>Michael Bay</td>
        </tr>
        <tr>
          <td>TV</td>
          <td>Sherlock, Firefly, Battlestar Galactica, Game of Thrones, Archer, Stranger Things</td>
          <td>Reality TV (Junk TV)</td>
        </tr>
        <tr>
          <td>Music</td>
          <td>
            Daft Punk, Ratatat, The Shins, Sigur Ros, Snow Patrol,
            <a href="http://www.brokenspindles.com/" target="_blank">Broken Spindles</a>,
            <a href="http://lunarmusic.net/" target="_blank">Lunar</a>,
            <a href="https://computeher.wordpress.com/" target="_blank">ComputeHer</a>
          </td>
          <td>Rap, Country</td>
        </tr>
        <tr>
          <td>Board Games</td>
          <td>
            <a href="http://dnd.wizards.com/" target="_blank">Dungeons & Dragons</a>,
            <a href="http://www.catan.com/" target="_blank">The Settlers of Catan<a/>
          </td>
          <td>Monopoly, The Game of Life</td>
        </tr>
        <tr>
          <td>Video Games</td>
          <td>Portal, Zelda, Half-Life, Mass Effect (1+2), Deus Ex: HR, Psychonauts, Dragon Age (1+3)</td>
          <td><a href="http://en.wikipedia.org/wiki/E.T._the_Extra-Terrestrial_%28video_game%29" target="_blank">E.T. the Extra-Terrestrial</a></td>
        </tr>
      </tbody>
    </table>
  </div>

  <style scoped>
    .highlight > tbody > tr:hover {
      background-color: #c8e6c9;
    }
  </style>
</dm-opinions>

<dm-contact>
  <div class="card-panel">
    <h5><i class="material-icons">perm_contact_calendar</i> Contact Info</h5>
    <address>
      <div class="row">
        <div class="col l6 s12">
          <abbr title="Email"><i class="material-icons tiny">email</i></abbr> <a href="mailto:donovan@donovan.mn">donovan@donovan.mn</a>
        </div>
        <div class="col l6 s12">
          <abbr title="Cell"><i class="material-icons tiny">contact_phone</i></abbr> <a href="tel:1-651-300-4287">(651) 300-4287</a>
        </div>
      </div>
    </address>
  </div>
</dm-contact>
